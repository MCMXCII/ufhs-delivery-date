<?php
class Ufhs_Deliverydate_Model_Resource_Orders_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('deliverydate/orders');
	}
}