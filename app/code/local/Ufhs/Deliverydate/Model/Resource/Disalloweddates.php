<?php
class Ufhs_Deliverydate_Model_Resource_Disalloweddates extends Mage_Core_Model_Resource_Db_Abstract
{
	public function _construct()
	{
		$this->_init('deliverydate/disallowed', 'id');
	}
}