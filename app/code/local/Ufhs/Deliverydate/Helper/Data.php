<?php

class Ufhs_Deliverydate_Helper_Data extends Mage_Core_Helper_Abstract
{
	static function getDeliveryDate($orderId)
	{
		$record = Mage::getModel('deliverydate/orders')->load($orderId, 'order_id');
		if ($record->getOrderId()) {
			return $record->getDate();
		} else {
			return '';
		}
	}
}