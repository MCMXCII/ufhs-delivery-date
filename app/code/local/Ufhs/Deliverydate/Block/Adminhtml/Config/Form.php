<?php
class Ufhs_Deliverydate_Block_Adminhtml_Config_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _construct()
	{
		parent::_construct();
		$this->setFormTitle('Settings');
	}

	protected function _prepareForm()
	{
		$form = new Varien_Data_Form(array(
			'id' => 'config',
			'action' => $this->getUrl('*/*/updateconfig'),
			'method' => 'post',
			'enctype' => 'multipart/form-data'
		));
		$form->setUseContainer(true);
		$this->setForm($form);

		$fieldset = $form->addFieldset('config', array(
			'legend' => Mage::helper('deliverydate')->__('Settings')
		));

		foreach (Mage::getModel('deliverydate/config')->getCollection()->getData() as $config) {
			$fieldset->addField(strtolower($config['id']), 'text', array(
				'label' => Mage::helper('deliverydate')->__($config['frontname']),
				'class' => 'required-entry',
				'required' => true,
				'name' => 'config[' . $config['id'] . ']',
				'value' => $config['value']
			));
		}

		return parent::_prepareForm();
	}
}