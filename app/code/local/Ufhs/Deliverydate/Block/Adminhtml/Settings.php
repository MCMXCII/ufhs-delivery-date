<?php

class Ufhs_Deliverydate_Block_Adminhtml_Settings extends Mage_Adminhtml_Block_Widget
{
	public function __construct()
	{
		parent::_construct();
	}

	public function getAllowedUrl()
	{
		return $this->getUrl('*/*/updateallowed');
	}

	public function getDays()
	{
		return Mage::getModel('deliverydate/alloweddays')->getCollection()->getData();
	}
}