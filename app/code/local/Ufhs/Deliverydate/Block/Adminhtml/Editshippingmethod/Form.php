<?php
class Ufhs_Deliverydate_Block_Adminhtml_Editshippingmethod_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _construct()
	{
		parent::_construct();
		$this->setFormTitle('Edit Shipping Method');
	}

	protected function _prepareForm()
	{
		$record = Mage::getModel('deliverydate/shippingmethods')->load($this->getRequest()->getParam('id'));

		$form = new Varien_Data_Form(array(
			'id' => 'editshippingmethod',
			'action' => $this->getUrl('*/*/updateshippingmethod'),
			'method' => 'post',
			'enctype' => 'multipart/form-data'
		));
		$form->setUseContainer(true);
		$this->setForm($form);

		$fieldset = $form->addFieldset('shipping_method', array(
			'legend' => Mage::helper('deliverydate')->__('Shipping Method Data')
		));

		$fieldset->addField('id', 'text', array(
			'label' => Mage::helper('deliverydate')->__('Code'),
			'class' => 'disabled',
			'required' => false,
			'disabled' => 'disabled',
			'value' => $record->getId()
		));

		$fieldset->addField('days', 'text', array(
			'label' => Mage::helper('deliverydate')->__('Days'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'days',
			'value' => $record->getDays()
		))->setType('number');

		$fieldset->addField('hiddenid', 'hidden', array(
			'name' => 'id',
			'value' => $record->getId()
		));

		return parent::_prepareForm();
	}
}