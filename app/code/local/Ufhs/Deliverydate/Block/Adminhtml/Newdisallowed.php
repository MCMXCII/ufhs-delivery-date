<?php
class Ufhs_Deliverydate_Block_Adminhtml_Newdisallowed extends Mage_Adminhtml_Block_Widget_Form_Container
{
	protected function _prepareLayout()
	{
		return parent::_prepareLayout();
	}

	public function __construct()
	{
		parent::__construct();
		$id = $this->getRequest()->getParam('id');
		$this->_objectId = 'id';
		$this->_blockGroup = 'deliverydate';
		$this->_controller = 'adminhtml';
		$this->_mode = 'newdisallowed';

		$this->_removeButton('save');
		$this->_removeButton('delete');
		$this->_removeButton('back');
		$this->_removeButton('reset');

		$this->addButton('new_save', [
			'label' => 'Add',
			'onclick' => "document.getElementById('newdisallowed').submit()",
			'class' => 'add'
		]);
	}

	public function getHeaderText()
	{
		return Mage::helper('deliverydate')->__('New Disallowed Date');
	}
}