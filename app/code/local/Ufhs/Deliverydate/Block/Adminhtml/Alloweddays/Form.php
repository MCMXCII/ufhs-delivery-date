<?php
class Ufhs_Deliverydate_Block_Adminhtml_Alloweddays_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _construct()
	{
		parent::_construct();
		$this->setFormTitle('Allowed Days');
	}

	protected function _prepareForm()
	{
		return parent::_prepareForm();
	}
}