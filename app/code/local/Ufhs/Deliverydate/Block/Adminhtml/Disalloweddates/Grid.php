<?php
class Ufhs_Deliverydate_Block_Adminhtml_Disalloweddates_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('deliverydateDisallowedGrid');
		$this->setDefaultSort('id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('deliverydate/disalloweddates')->getCollection();
		$collection->setOrder('id', 'ASC');
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('date', array(
			'header' => Mage::helper('deliverydate')->__('Date'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'date'
		));
		$this->addColumn('reason', array(
			'header' => Mage::helper('deliverydate')->__('Reason'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'reason'
		));
		$this->addColumn('recurring', array(
			'header' => Mage::helper('deliverydate')->__('Recurring'),
			'align' => 'left',
			'width' => '50px',
			'index' => 'recurring'
		));
		$this->addColumn('action', array(
			'header'    => '',
			'width'     => '50px',
			'type'      => 'action',
			'getter'     => 'getId',
			'actions'   => array(
				array(
					'caption' => Mage::helper('deliverydate')->__('Delete'),
					'url'     => array(
						'base'=>'*/deliverydate/deletedisallowed'
					),
					'field'   => 'id'
				)
			),
			'filter'    => false,
			'sortable'  => false,
			'index'     => 'stores',
		));

		$object = new Varien_Object(array('grid_block' => $this));
		Mage::dispatchEvent("deliverydate_block_adminhtml_disalloweddates_grid_preparecolumns", array("data" => $object));
		return parent::_prepareColumns();
	}

	public function getEmptyText()
	{
		return $this->__('No dates have been disallowed yet.');
	}
}