<?php
class Ufhs_Deliverydate_Block_Adminhtml_Colours_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _construct()
	{
		parent::_construct();
		$this->setFormTitle('Colour Scheme');
	}

	protected function _prepareForm()
	{
		$form = new Varien_Data_Form(array(
			'id' => 'colours',
			'action' => $this->getUrl('*/*/updatecolours'),
			'method' => 'post',
			'enctype' => 'multipart/form-data'
		));
		$form->setUseContainer(true);
		$this->setForm($form);

		$fieldset = $form->addFieldset('colours', array(
			'legend' => Mage::helper('deliverydate')->__('Styles')
		));

		foreach (Mage::getModel('deliverydate/colours')->getCollection()->getData() as $colour) {
			$fieldset->addField(strtolower($colour['label']), 'text', array(
				'label' => Mage::helper('deliverydate')->__($colour['label']),
				'class' => 'required-entry',
				'required' => true,
				'name' => 'colour[' . $colour['id'] . ']',
				'value' => $colour['colour']
			))->setType('color');
		}

		return parent::_prepareForm();
	}
}