<?php
class Ufhs_Deliverydate_Block_Adminhtml_Orders extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_orders';
		$this->_blockGroup = 'deliverydate';
		$this->_headerText = Mage::helper('deliverydate')->__('Orders');
		parent::__construct();
		$this->_updateButton('add', 'onclick',"setLocation('" . $this->getUrl('*/*/neworder') . "')");
	}
}