<?php
class Ufhs_Deliverydate_Block_Adminhtml_Shippingmethods extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_shippingmethods';
		$this->_blockGroup = 'deliverydate';
		$this->_headerText = Mage::helper('deliverydate')->__('Shipping Methods');
		parent::__construct();

		$this->_removeButton('add');

		$this->addButton('new_save', [
			'label' => 'Sync',
			'onclick' => "setLocation('" . $this->getUrl('*/*/syncshippingmethods') . "')",
			'class' => 'add'
		]);
	}
}