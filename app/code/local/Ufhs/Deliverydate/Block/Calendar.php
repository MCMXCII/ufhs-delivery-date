<?php

class Ufhs_Deliverydate_Block_Calendar extends Mage_Core_Block_Template
{
	public function __construct() {
		parent::__construct();
	}

	public function getDisallowedDates()
	{
		$collection = Mage::getModel('deliverydate/disalloweddates')->getCollection()->getData();
		$return = [];

		foreach ($collection  as $date) {
			$index = (new DateTime($date['date']))->format('j/n/Y');
			$return[$index] = $date['reason'];
		}
		return $return;
	}

	public function getOptionalDays()
	{
		$collection = Mage::getModel('deliverydate/alloweddays')->getCollection()
		->addFieldToFilter('optional', 1)
		->getData();
		$return = [];
		foreach ($collection as $day) {
			$return[$day['id']] = $day['reason'];
		}
		return $return;
	}

	public function getDisallowedDays()
	{
		$collection = Mage::getModel('deliverydate/alloweddays')->getCollection()
		->addFieldToFilter('allowed', 0)
		->getData();
		$return = [];
		foreach ($collection as $day) {
			$return[] = $day['id'];
		}
		return $return;
	}

	public function getColour($type)
	{
		return Mage::getModel('deliverydate/colours')->load($type, 'label')->getColour();
	}

	public function getDatePickerScript($optional = true)
	{
		if ($optional) {
			return $this->getLayout()->createBlock('deliverydate/calendar')->setTemplate('deliverydate/datepicker-script.phtml')->toHtml();
		} else {
			return $this->getLayout()->createBlock('deliverydate/calendar')->setTemplate('deliverydate/datepicker-script-selectable-optionals.phtml')->toHtml();
		}
	}

	public function getCutoffTime()
	{
		$config = Mage::getModel('deliverydate/config')->load('cutoff')->getValue();
		return $config && (int)$config > 0 && (int)$config < 25 ? $config : null;
	}

	public function getPackageDelay()
	{
		$packageAttrib = Mage::getModel('deliverydate/config')->load('shipping-attribute')->getValue();
		if (!$packageAttrib) {
			return null;
		}
		$packages = $return = [];
		$cart = Mage::getModel('checkout/cart')->getQuote();
		foreach ($cart->getAllItems() as $item) {
			$package = $item->getProduct()->getAttributeText('package_id');
			if (!isset($return[$package])) {
				$return[$package] = Mage::getModel('deliverydate/shippingmethods')->load($package)->getDays();
			}
		}
		return max($return);
	}
}