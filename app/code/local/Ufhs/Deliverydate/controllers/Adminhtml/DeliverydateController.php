<?php

class Ufhs_Deliverydate_Adminhtml_DeliverydateController extends Mage_Adminhtml_Controller_action
{
	protected function _initAction()
	{
		$this->_title($this->__('Delivery Date'));
		$this->loadLayout();
		$this->_initLayoutMessages('adminhtml/session');
		$this->_setActiveMenu('ufhs');
		return $this;
	}

	private function _outputLayout($title)
	{
		$this->_initAction();
		$this->_title($this->__($title));
		$this->renderLayout();
	}

	/**
	 * Layout Functions
	 */
	public function viewdataAction()
	{
		$this->_outputLayout('View Orders');
	}

	public function dailydeliveryAction()
	{
		$this->_outputLayout('Daily Delivery');
	}

	public function nondeliverydatesAction()
	{
		$this->_outputLayout('Non Delivery Dates');
	}

	public function shippingmethodsAction()
	{
		$this->_outputLayout('Shipping Methods');
	}

	public function configAction()
	{
		$this->_outputLayout('Settings');
	}

	public function neworderAction()
	{
		$this->_outputLayout('New Order');
	}

	public function editshippingmethodAction()
	{
		$this->_outputLayout('Edit Shipping Method');
	}

	public function updateallowedAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (isset($post['days'])) {
			foreach ($post['days'] as $id => $day) {
				Mage::getModel('deliverydate/alloweddays')->load($id)
				->setAllowed(isset($day['allowed']))
				->setOptional(isset($day['optional']))
				->setReason($day['reason'])
				->save();
			}
		} else {
			Mage::getSingleton('adminhtml/session')->addError("We seem to be missing a days array, stop messing with the code.");
		}
		return $this->_redirect('*/*/dailydelivery');
	}

	public function adddisallowedAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (isset($post['date'])) {
			Mage::getModel('deliverydate/disalloweddates')
			->setDate($post['date'])
			->setReason($post['reason'])
			->setRecurring(isset($post['recurring']))
			->save();
		}
		return $this->_redirect('*/*/nondeliverydates');
	}

	public function updatecoloursAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (isset($post['colour'])) {
			foreach ($post['colour'] as $id => $colour) {
				Mage::getModel('deliverydate/colours')->load($id)
				->setColour($colour)
				->save();
			}
		} else {
			Mage::getSingleton('adminhtml/session')->addError("We seem to be missing colours, stop messing with the code.");
		}
		return $this->_redirect('*/*/config');
	}

	public function updateconfigAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (isset($post['config'])) {
			foreach ($post['config'] as $id => $config) {
				Mage::getModel('deliverydate/config')->load($id)
				->setValue($config)
				->save();
			}
		} else {
			Mage::getSingleton('adminhtml/session')->addError("We seem to be missing a config node, stop messing with the code.");
		}
		return $this->_redirect('*/*/config');
	}

	public function deletedisallowedAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (isset($post['id'])) {
			Mage::getModel('deliverydate/disalloweddates')->load($post['id'])
			->delete();
		} else {
			Mage::getSingleton('adminhtml/session')->addError("We are missing an ID, trying POST'ing to this page.");
		}
		return $this->_redirect('*/*/nondeliverydates');
	}

	public function addorderAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (isset($post['order_id']) && isset($post['date'])) {
			$order = Mage::getModel('sales/order')->loadByIncrementId($post['order_id']);
			if ($order->getId()) {
				$record = Mage::getModel('deliverydate/orders')->load($post['order_id'], 'order_id');
				if (!$record->getDate()) {
					Mage::getModel('deliverydate/orders')->setOrderId($post['order_id'])->setDate($post['date'])->save();
					return $this->_redirect('*/*/viewdata');
				} else {
					Mage::getSingleton('adminhtml/session')->addError("That order has already been defined.");
				}
			} else {
				Mage::getSingleton('adminhtml/session')->addError("That isn't a real order.");
			}
		} else {
			Mage::getSingleton('adminhtml/session')->addError("We seem to be missing some information, please try again.");
		}
		return $this->_redirect('*/*/neworder');
	}

	public function syncshippingmethodsAction()
	{
		$attribute = Mage::getModel('deliverydate/config')->load('shipping-attribute')->getValue();
		if ($attribute) {
			$attributeSet = Mage::getSingleton('eav/config')->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attribute);
			if ($attributeSet->getId()) {
				foreach ($attributeSet->getSource()->getAllOptions() as $value) {
					if (!Mage::getModel('deliverydate/shippingmethods')->load($value['label'])->getId()) {
						Mage::getModel('deliverydate/shippingmethods')
						->setData(['id' => $value['label'], 'days' => 1])->save();
					}
				}
			} else {
				Mage::getSingleton('adminhtml/session')->addError("There aren't any shipping methods assigned to that attribute. Please update the configuration and try again.");
			}
		} else {
			Mage::getSingleton('adminhtml/session')->addError("We seem to be missing an attribute to download the shipping methods against.");
		}
		return $this->_redirect('*/*/shippingmethods');
	}

	public function updateshippingmethodAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (!empty($post['id']) && !empty($post['days'])) {
			$item = Mage::getModel('deliverydate/shippingmethods')->load($post['id']);
			if ($item->getId()) {
				$item->setDays($post['days'])->save();
			} else {
				Mage::getSingleton('adminhtml/session')->addError("There doesn't seem to be a record of that shipping method. Chances are you changed the URL and it broke everything. Try going back a page.");
			}
		} else {
			Mage::getSingleton('adminhtml/session')->addError("We seem to be missing some data, stop messing with the code.");
		}
		return $this->_redirect('*/*/shippingmethods');
	}
}