<?php

class Ufhs_Deliverydate_DateController extends Mage_Core_Controller_Front_Action
{
	public function updateAction()
	{
		$post = Mage::app()->getRequest()->getParams();
		if (isset($post['date'])) {
			return Mage::getSingleton('core/session')->setData('deliveryData', $post['date']);
		}
	}
}